#include <my_global.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <string.h>
#include <mysql.h>

MYSQL *con;

struct userLoginData
{
	char username[100];
	char password[100];
};

struct userData
{
	char username[100];
	char password[100];

	int userSocket;
	int userID;
	pthread_t userThread;
};

struct userData users[100];

int userCount = 0;

void *userThread(void *params);
char *getFileList(struct userData *connectionData);
char* getUsersList(struct userData *connectionData);
void sendFile(char* fileName, struct userData *connectionData);
void receiveFile(char* fileName, struct userData *connectionData);
void createAccount(struct userData *connectionData);
void login(struct userData *connectionData);
void deleteFile(char *path, struct userData *connectionData);
void deleteUser(char *userName, struct userData *connectionData);

int main()
{
	int port = 3000;
	int active = 1;
	int serverSocket = socket(AF_INET, SOCK_STREAM, 0);
	int option = 1;
	setsockopt (serverSocket, SOL_SOCKET, SO_REUSEADDR, (void *) &option, sizeof (option));

	struct sockaddr_in clientAddress;
	struct sockaddr_in serverAddress;

	serverAddress.sin_family = AF_INET;
	serverAddress.sin_addr.s_addr = htonl(INADDR_ANY);
	serverAddress.sin_port = htons(port);

	con = mysql_init(NULL);

	if (mysql_real_connect(con, "localhost", "root", "Darkness0", "userData", 0, NULL, 0) == NULL) 
	{
		fprintf(stderr, "%s\n", mysql_error(con));
		mysql_close(con);
	}  

	printf("MySQL client version: %s\n", mysql_get_client_info());

	int serverAddressSize;
	if (bind (serverSocket, (struct sockaddr *) &serverAddress, sizeof(struct sockaddr)) == -1)
	{
		printf("Can't bind !\n");
	}

	if (listen(serverSocket, 5) == -1)
	{
		printf("Can't listen !\n");
	}

	while(active)
	{	
		int clientAddressSize = sizeof(struct sockaddr_in);
		int clientSocket = accept(serverSocket, (struct sockaddr *) &clientAddress, &clientAddressSize);

		if (clientSocket < 0)
		{
			printf("Error at accept !\n");
		}

		pthread_t tempUserThread;

		struct userData newUser;
		newUser.userID = userCount;
		newUser.userThread = tempUserThread;
		newUser.userSocket = clientSocket;

		users[userCount] = newUser;

		pthread_create(&users[userCount].userThread, NULL, (void*) userThread, (void*) &users[userCount]);
		userCount++;

	}

	return 0;
}

void *userThread(void *params)
{
	struct userData *data = (struct userData*) params;
	int active = 1;

	while(active)
	{
		char *command;
		int commandSize = -1;

		if (read(data->userSocket, &commandSize, sizeof(int)) <= 0)
		{	
			printf("User disconnected !\n");
			userCount--;
			return ;
		}

		command = calloc(sizeof(char), commandSize);

		if (read(data->userSocket, command, sizeof(char) * commandSize) <= 0)
		{	
			printf("User disconnected !\n");
			userCount--;
			return ;
		}
		else
		{
			if (strcmp(command, "login") == 0)
			{
				login(data);
			}
			else if (strcmp(command, "createAccount") == 0)
			{
				createAccount(data);
			}
			else if (strcmp(command, "list") == 0)
			{
				char *fileList = NULL;

				fileList = getFileList(data);

				int size = strlen(fileList);

				if (write(data->userSocket, &size, sizeof(int)) <= 0)
				{
					printf("Can't send data to client ! [size of fileList]\n");
				}

				if (write(data->userSocket, fileList, strlen(fileList) * sizeof(char)) <= 0)
				{
					printf("Can't send data to client ! [fileList]\n");
				}
				free(fileList);
			}
			else if (strcmp(command, "usersList") == 0)
			{
				char *usersList = NULL;

				usersList = getUsersList(data);

				int size = strlen(usersList);

				if (write(data->userSocket, &size, sizeof(int)) <= 0)
				{
					printf("Can't send data to client ! [size of usersList]\n");
				}

				if (write(data->userSocket, usersList, strlen(usersList) * sizeof(char)) <= 0)
				{
					printf("Can't send data to client ! [usersList]\n");
				}
				free(usersList);
			}
			else if (strcmp(command, "download") == 0)
			{
				int fileNameLength;

				if (read(data->userSocket, &fileNameLength, sizeof(int)) < 0)
				{
					printf("Can't read data from client ! [file name size]\n");
				}

				char* fileName = calloc(fileNameLength, sizeof(char));

				if (read(data->userSocket, fileName, fileNameLength * sizeof(char)) < 0)
				{
					printf("Can't read data from client ! [file name size]\n");
					return ;
				}

				sendFile(fileName, data);
				free(fileName);
			}	
			else if (strcmp(command, "upload") == 0)
			{
				int fileNameLength;

				if (read(data->userSocket, &fileNameLength, sizeof(int)) < 0)
				{
					printf("Can't read data from client ! [upload 1]\n");
				}

				char* fileName = calloc(fileNameLength, sizeof(char));

				if (read(data->userSocket, fileName, fileNameLength * sizeof(char)) < 0)
				{
					printf("Can't read data from client ! [upload 2]\n");
				}

				receiveFile(fileName, data);
				free(fileName);
			}
			else if (strcmp(command, "deleteFile") == 0)
			{
				int size;
				char* path ;

				if (read(data->userSocket, &size, sizeof(int)) < 0)
				{
					printf("Can't read data from client ! [deleteFile 1]\n");
				}

				path = calloc(size, sizeof(char));

				if (read(data->userSocket, path, size * sizeof(char)) < 0)
				{
					printf("Can't read data from client ! [deleteFile 2]\n");
				}
				deleteFile(path, data);
				free(path);
			}
			else if (strcmp(command, "deleteUser") == 0)
			{
				int size;
				char* userName;

				if (read(data->userSocket, &size, sizeof(int)) < 0)
				{
					printf("Can't read data from client ! [deleteUser 1]\n");
				}

				userName = calloc(size, sizeof(char));

				if (read(data->userSocket, userName, size * sizeof(char)) < 0)
				{
					printf("Can't read data from client ! [deleteUser 2]\n");
				}
				deleteUser(userName, data);
				free(userName);
			}
			free(command);
		}
	}
}

char* getFileList(struct userData *connectionData)
{
	DIR *userDir = opendir(connectionData->username);

	if (userDir)
	{
		char *fileList = calloc (sizeof(char), 1);
		struct dirent *fileData = NULL;
		int empty = 1;

		while(fileData = readdir(userDir))
		{
			if (strcmp(fileData->d_name, ".") != 0 && strcmp(fileData->d_name, "..") != 0)
			{
				int newSize =  sizeof(char) * (strlen(fileList) + strlen(fileData->d_name) + 2);
				empty = 0;

				fileList = realloc(fileList, newSize);
				strcat(fileList, "\n");
				strcat(fileList, fileData->d_name);
			}
		}
		closedir(userDir);

		if (empty == 1)
		{
			char* aux = "No files found !";
			fileList = realloc(fileList, strlen(aux) * sizeof(char));
			strcpy(fileList, aux);
		}

		return fileList;
	}
}

char* getUsersList(struct userData *connectionData)
{
	char *usersList = calloc (sizeof(char), 1);
	struct dirent *fileData = NULL;
	int empty = 1;

	char query[400] = "select name from users;";

	if(mysql_query(con, query))
	{
		printf("MySQL query error !\n");
		return ;
	}

	MYSQL_RES *result = mysql_store_result(con);
	int num_fields = mysql_num_fields(result);

	MYSQL_ROW row;

	while ((row = mysql_fetch_row(result))) 
	{	 
		empty = 0;
		for(int i = 0; i < num_fields; i++) 
		{ 
			printf("row %s\n", row[0]);
			usersList = realloc(usersList, sizeof(char) * (strlen(usersList) + strlen(row[0]) + 2));
			strcat(usersList, "\n");
			strcat(usersList, row[0]);
		} 
	}

	if (empty == 1)
	{
		char* aux = "No users found !";
		usersList = realloc(usersList, strlen(aux) * sizeof(char));
		strcpy(usersList, aux);
	}

	return usersList;
}

void sendFile(char* fileName, struct userData *connectionData)
{
	DIR *userDir = opendir(connectionData->username);

	if (userDir)
	{
		struct dirent *fileData = NULL;
		int found = 0;

		while((fileData = readdir(userDir)) && found == 0)
		{
			if (strcmp(fileData->d_name, fileName) == 0)
			{
				found = 1;
				struct stat fileStat;

				printf("File name: %s\n", fileName);

				char* path = malloc(strlen(connectionData->username) + strlen(fileName) + 1);
				strcpy(path, connectionData->username);
				strcat(path, "/");
				strcat(path, fileName);

				if (stat(path, &fileStat) < 0)
				{
					printf("Error at stat !\n");
					return ;
				}

				int size = fileStat.st_size;

				int blockSize = 10240 * 5;
				int blocksOfData = size / blockSize; // Every block will have 10 KB of data

				if (write(connectionData->userSocket, &blockSize, sizeof(int)) < 0)
				{
					printf("Can't send data to client !\n");
					return ;
				}

				printf("Blocks: %d\n", blocksOfData);

				if (write(connectionData->userSocket, &blocksOfData, sizeof(int)) < 0)
				{
					printf("Can't send data to client !\n");
					return ;
				}

				int file;
				if ((file = open(path, O_RDONLY)) < 0)
				{
					printf("Can't open file !\n");
					return ;
				}

				for (unsigned int i = 0; i < blocksOfData; i++)
				{
					char blockOfData[blockSize];

					if (read(file, blockOfData, blockSize) < 0)
					{
						printf("Can't read data from client !\n");
						return ;
					}	

					if (write(connectionData->userSocket, &blockOfData, sizeof(char) * blockSize) < 0)
					{
						printf("Can't send data to client !\n");
						return ;
					}
				}

				int rest = size - (blocksOfData * blockSize);
				char blockOfData[rest];

				if (write(connectionData->userSocket, &rest, sizeof(int)) < 0)
				{
					printf("Can't send data to client !\n");
					return ;
				}

				if (read(file, blockOfData, rest) < 0)
				{
					printf("Can't read data from client !\n");
					return ;
				}

				if (write(connectionData->userSocket, &blockOfData, sizeof(char) * rest) < 0)
				{
					printf("Can't send data to client !\n");
					return ;
				}

				printf("Success !\n");

				close(file);
			}
		}

		if (!found)
		{
			int response = -1;
			write(connectionData->userSocket, &response, sizeof(int));
		}
		
		closedir(userDir);
	}
}

void receiveFile(char* fileName, struct userData *connectionData)
{
	int blocksOfData;
	int blockSize;
	int file;
	int rest;
	char* filePath;

	if (read(connectionData->userSocket, &blockSize, sizeof(int)) < 0)
	{
		printf("Can't send data to server ! [upload 3]\n");
		return ;
	}

	if (read(connectionData->userSocket, &blocksOfData, sizeof(int)) < 0)
	{
		printf("Can't send data to server ! [upload 4]\n");
		return ;
	}

	filePath = (char*) malloc(strlen(connectionData->username) + strlen(fileName) + 1);
	strcpy(filePath, connectionData->username);
	strcat(filePath, "/");
	strcat(filePath, fileName);

	if ((file = open(filePath, O_WRONLY | O_CREAT, 0777)) < 0)
	{
		printf("Can't open file ! [upload 5]\n");
		return ;
	}

	for (int i = 0; i < blocksOfData; i++)
	{
		char blockOfData[blockSize];

		if (read(connectionData->userSocket, blockOfData, blockSize) < 0)
		{
			printf("Can't read data from server ! [upload 6]\n");
			return ;
		}

		if (write(file, blockOfData, blockSize) < 0)
		{
			printf("Can't write to file ! [upload 7]\n");
			return ;
		}
	}

	if (read(connectionData->userSocket, &rest, sizeof(int)) < 0)
	{
		printf("Can't read data from server ! [upload 8]\n");
		return ;
	}

	char blockOfData[rest];

	if (read(connectionData->userSocket, blockOfData, rest) < 0)
	{
		printf("Can't read data from server ! [upload 9]\n");
		return ;
	}

	if (write(file, blockOfData, rest) < 0)
	{
		printf("Can't write to file ! [upload 10]\n");
		return ;
	}

	close(file);
}

void createAccount(struct userData *connectionData)
{
	struct userLoginData accountData;
	int response = 1;

	if (read(connectionData->userSocket, &accountData, sizeof(struct userLoginData)) < 0)
	{
		printf("Can't read data from server ! [create account 1]\n");
		response = -1;
	}	

	strcpy(connectionData->username, accountData.username);
	strcpy(connectionData->password, accountData.password);

	char query[400] = "select * from users where name = '";
	strcat(query, connectionData->username);
	strcat(query, "';");

	if(mysql_query(con, query))
	{
		printf("MySQL query error !\n");
	}

	MYSQL_RES *result = mysql_store_result(con);
	int num_fields = mysql_num_fields(result);

	MYSQL_ROW row;

	while ((row = mysql_fetch_row(result))) 
	{	 
		if (strcmp(row[1], connectionData->username) == 0)
		{
			response = 0;
		}
	}

	if (!response)
	{
		write(connectionData->userSocket, &response, sizeof(int));
	}
	else
	{
		char query2[400] = "insert into users (name, password, isAdmin) values ('";
		strcat(query2, connectionData->username);
		strcat(query2, "', '");
		strcat(query2, connectionData->password);
		strcat(query2, "', '0');");

		if(mysql_query(con, query2))
		{
			printf("MySQL query error 2 !\n");
			response = 0;
		}
		mkdir(connectionData->username, 0777);
		write(connectionData->userSocket, &response, sizeof(int));
	}
}

void login(struct userData *connectionData)
{
	int response = -4;
	struct userLoginData data;
	int isAdmin = 0;

	if (read(connectionData->userSocket, &data, sizeof(struct userLoginData)) <= 0)
	{
		printf("Can't read data from client ! [main]\n");
	}

	char decodedPassword[strlen(data.password) / 2];

	//printf("Pass %s\n", data.password[0]);
	char pass[100];
	strncpy(pass, data.password, strlen(data.password) + 1);
	char key = '+';
	int index = 0;

	for (unsigned int i = 0; i < strlen(pass); i += 2)
	{
		char aux = pass[i] ^ key;
		decodedPassword[index] = aux;
		index++;
	}
	decodedPassword[index] = '\0';

	strcpy(connectionData->username, data.username);
	strcpy(connectionData->password, decodedPassword);

	char query[400] = "select * from users where name = '";
	strcat(query, connectionData->username);
	strcat(query, "' and password = '");
	strcat(query, connectionData->password);
	strcat(query, "';");

	if(mysql_query(con, query))
	{
		printf("MySQL query error !\n");
		return ;
	}

	MYSQL_RES *result = mysql_store_result(con);
	int num_fields = mysql_num_fields(result);

	MYSQL_ROW row;

	while ((row = mysql_fetch_row(result))) 
	{	 
		
		if (strcmp(row[1], connectionData->username) == 0 && strcmp(row[2], connectionData->password) == 0)
		{
			response = 1;
		}

		if (strcmp(row[3], "1") == 0)
		{
			isAdmin = 1;
		}

	}

	if (write(connectionData->userSocket, &isAdmin, sizeof(int)) < 0)
	{
		printf("Can't send data to client ! [login]\n");
	}

	if (write(connectionData->userSocket, &response, sizeof(int)) < 0)
	{
		printf("Can't send data to client ! [login]\n");
	}
}

void deleteFile(char *path, struct userData *connectionData)
{
	int response = 1;
	char* pathToDelete = malloc(strlen(path) + strlen(connectionData->username) + 1);
	strcpy(pathToDelete, connectionData->username);
	strcat(pathToDelete, "/");
	strcat(pathToDelete, path);

	if (remove(pathToDelete) != 0)
	{
		response = -1;
	}

	if (write(connectionData->userSocket, &response, sizeof(int)) < 0)
	{
		printf("Can't send data to client ! [login]\n");
	}
}

void deleteUser(char *userName, struct userData *connectionData)
{
	int response = 1;

	char query[400] = "delete from users where name = '";
	strcat(query, userName);
	strcat(query, "';");

	printf("%s\n", query);

	if(mysql_query(con, query))
	{
		printf("MySQL query error !\n");
		response = 0;
	}

	if (write(connectionData->userSocket, &response, sizeof(int)) < 0)
	{
		printf("Can't send data to client ! [login]\n");
	}
}