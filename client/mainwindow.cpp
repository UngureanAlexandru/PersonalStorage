#include "mainwindow.h"

#include <iostream>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent)
{
    clientSocket = socket(AF_INET, SOCK_STREAM, 0);

    clientAddress.sin_family = AF_INET;
    clientAddress.sin_addr.s_addr = inet_addr(ip);
    clientAddress.sin_port = htons(port);

    if (::connect(clientSocket, (struct sockaddr *) &clientAddress, sizeof(struct sockaddr)) < 0)
    {
        loginResultLabel->setText("Can't connect to server !");
    }

    selectedDownloadPath = 0;
    selectedUploadPath = 0;

    ////////////////////// Login Panel /////////////////////////

    loginPanel = new QGroupBox();

    usernameLogin = new QLabel("Username: ");
    usernameInput = new QLineEdit;

    passwordLogin = new QLabel("Password: ");
    passwordInput = new QLineEdit;

    loginLayout = new QFormLayout;

    loginButton = new QPushButton("Login");
    goToCreateAccountButton = new QPushButton("Create account");

    fileDialog = new QFileDialog;

    loginResultLabel = new QLabel();

    setCentralWidget(loginPanel);

    loginPanel->setLayout(loginLayout);

    passwordInput->setEchoMode(QLineEdit::Password);

    loginLayout->addRow(usernameLogin, usernameInput);
    loginLayout->addRow(passwordLogin, passwordInput);
    loginLayout->addRow(loginButton);
    loginLayout->addRow(goToCreateAccountButton);
    loginLayout->addRow(loginResultLabel);

    connect(loginButton, SIGNAL(clicked()), this, SLOT(login()));
    connect(goToCreateAccountButton, SIGNAL(clicked()), this, SLOT(goToCreateAccount()));

    ////////////////////////// Download Panel ////////////////////////

    downloadPanel = new QGroupBox;
    commandText = new QLabel("Command:");
    commandOutput = new QLabel("Output:");
    commandInput = new QLineEdit;
    sendCommandButton = new QPushButton("Send button");
    setDownloadLocationButton = new QPushButton("Set download location");
    setUploadLocationButton = new QPushButton("Set upload location");

    downloadPanelLayout = new QFormLayout;

    downloadPanel->setLayout(downloadPanelLayout);
    downloadPanelLayout->addRow(commandText, commandInput);
    downloadPanelLayout->addRow(sendCommandButton);
    downloadPanelLayout->addRow(setDownloadLocationButton);
    downloadPanelLayout->addRow(setUploadLocationButton);
    downloadPanelLayout->addRow(commandOutput);

    connect(sendCommandButton, SIGNAL(clicked()), this, SLOT(sendCommand()));
    connect(setDownloadLocationButton, SIGNAL(clicked()), this, SLOT(setDownloadLocation()));
    connect(setUploadLocationButton, SIGNAL(clicked()), this, SLOT(setUploadLocation()));

    ///////////////////////// Create account //////////////////////////////

    createAccountPanel = new QGroupBox;
    createAccountPanelLayout = new QFormLayout;

    newUsernameLabel = new QLabel("Username:");
    newUsernameInput = new QLineEdit;

    newPasswordLabel = new QLabel("Password:");
    newPasswordInput = new QLineEdit;

    createAccountButton = new QPushButton("Create account");
    backButton = new QPushButton("Back");

    createAccountResult = new QLabel;

    createAccountPanel->setLayout(createAccountPanelLayout);

    newPasswordInput->setEchoMode(QLineEdit::Password);

    createAccountPanelLayout->addRow(newUsernameLabel, newUsernameInput);
    createAccountPanelLayout->addRow(newPasswordLabel, newPasswordInput);
    createAccountPanelLayout->addRow(createAccountButton);
    createAccountPanelLayout->addRow(backButton);
    createAccountPanelLayout->addRow(createAccountResult);

    connect(backButton, SIGNAL(clicked()), this, SLOT(back()));
    connect(createAccountButton, SIGNAL(clicked()), this, SLOT(createAccount()));

    //////////////////////////////////////////////////////////////////////
}

void MainWindow::login()
{
    QString usernameField = usernameInput->text();
    QString passwordField = passwordInput->text();

    std::string username = usernameField.toStdString();
    std::string password = passwordField.toStdString();

    int validData = 1;

    for (unsigned int i = 0; i < username.length(); i++)
    {
        char c;
        int valid = 0;

        for (c = 'a'; c <= 'z'; c++)
        {
            if (username[i] == c)
                valid = 1;
        }

        for (c = 'A'; c <= 'Z'; c++)
        {
            if (username[i] == c)
                valid = 1;
        }

        for (c = '0'; c <= '9'; c++)
        {
            if (username[i] == c)
                valid = 1;
        }

        if (!valid)
        {
            validData = 0;
            break;
        }
    }

    for (unsigned int i = 0; i < password.length(); i++)
    {
        char c;
        int valid = 0;

        for (c = 'a'; c <= 'z'; c++)
        {
            if (password[i] == c)
                valid = 1;
        }

        for (c = 'A'; c <= 'Z'; c++)
        {
            if (password[i] == c)
                valid = 1;
        }

        for (c = '0'; c <= '9'; c++)
        {
            if (password[i] == c)
                valid = 1;
        }

        if (!valid)
        {
            validData = 0;
            break;
        }
    }

    if (!validData)
    {
        loginResultLabel->setText("Invalid username or password ! Please use only letters !");
        return ;
    }

    std::string cryptedPassword;

    char *command;
    int size;
    int result = -5;

    for (unsigned int i = 0; i < password.length(); i++)
    {
        cryptedPassword += password[i];
        if (i < 125)
        {
            cryptedPassword += password[i] + i;
        }
        else
        {
            cryptedPassword += password[i] + 125;
        }
    }

    for (unsigned int i = 0; i < cryptedPassword.length(); i++)
    {
        char aux = cryptedPassword[i] ^ '+';
        cryptedPassword[i] = aux;
    }

    struct userData data;
    strcpy(data.username, username.c_str());
    strcpy(data.password, cryptedPassword.c_str());

    size = strlen("login");
    command = (char*) calloc(sizeof(char), size);
    strcpy(command, "login");

    if (write(clientSocket, &size, sizeof(int)) <= 0)
    {
        result = -2;
        loginResultLabel->setText("Can't send data to server !");
    }

    if (write(clientSocket, command, sizeof(char) * size) < 0)
    {
        result = -2;
        loginResultLabel->setText("Can't send data to server !");
    }

    if (write(clientSocket, &data, sizeof(struct userData)) < 0)
    {
        result = -2;
        loginResultLabel->setText("Can't send data to server !");
    }

    if (read(clientSocket, &isAdmin, sizeof(int)) < 0)
    {
        result = -3;
        loginResultLabel->setText("Can't read data from server !");
    }

    if (read(clientSocket, &result, sizeof(int)) < 0)
    {
        result = -3;
        loginResultLabel->setText("Can't read data from server !");
    }

    if (result == -1)
    {
        loginResultLabel->setText("Can't connect to server !");
    }

    if (result == -4)
    {
        loginResultLabel->setText("User not found !");
    }

    if (result > 0)
    {
        loginPanel->setVisible(false);
        downloadPanel->setVisible(true);
    }
}

void MainWindow::goToCreateAccount()
{
    loginPanel->setVisible(false);
    createAccountPanel->setVisible(true);
}

void MainWindow::createAccount()
{
    struct userData loginData;
    int result = -1;
    char* command = (char*) "createAccount";
    int size = strlen(command);

    int nameSize = ::strlen(newUsernameInput->text().toStdString().c_str());
    int passwordSize = ::strlen(newPasswordInput->text().toStdString().c_str());

    if (nameSize > 50)
    {
        createAccountResult->setText("Username can't have more than 50 characters !");
        return ;
    }

    if (passwordSize > 50)
    {
        createAccountResult->setText("Password can't have more than 50 characters !");
        return ;
    }

    if (nameSize == 0 || passwordSize == 0)
    {
        createAccountResult->setText("All fields must be filled !");
        return ;
    }

    int validData = 1;

    for (unsigned int i = 0; i < newUsernameInput->text().toStdString().length(); i++)
    {
        char c;
        int valid = 0;

        for (c = 'a'; c <= 'z'; c++)
        {
            if (newUsernameInput->text().toStdString()[i] == c)
                valid = 1;
        }

        for (c = 'A'; c <= 'Z'; c++)
        {
            if (newUsernameInput->text().toStdString()[i] == c)
                valid = 1;
        }

        for (c = '0'; c <= '9'; c++)
        {
            if (newUsernameInput->text().toStdString()[i] == c)
                valid = 1;
        }

        if (!valid)
        {
            validData = 0;
            break;
        }
    }

    for (unsigned int i = 0; i < newPasswordInput->text().toStdString().length(); i++)
    {
        char c;
        int valid = 0;

        for (c = 'a'; c <= 'z'; c++)
        {
            if (newPasswordInput->text().toStdString()[i] == c)
                valid = 1;
        }

        for (c = 'A'; c <= 'Z'; c++)
        {
            if (newPasswordInput->text().toStdString()[i] == c)
                valid = 1;
        }

        for (c = '0'; c <= '9'; c++)
        {
            if (newPasswordInput->text().toStdString()[i] == c)
                valid = 1;
        }

        if (!valid)
        {
            validData = 0;
            break;
        }
    }

    if (!validData)
    {
        createAccountResult->setText("Invalid username or password ! Please use only letters !");
        return ;
    }

    ::strcpy(loginData.username, newUsernameInput->text().toStdString().c_str());
    ::strcpy(loginData.password, newPasswordInput->text().toStdString().c_str());

    if (write(clientSocket, &size, sizeof(int)) < 0)
    {
        std::cout << "Can't send data to server ! [create account 1]" << std::endl;
        return ;
    }

    if (write(clientSocket, command, size * sizeof(char)) < 0)
    {
        std::cout << "Can't send data to server ! [create account 2]" << std::endl;
        return ;
    }

    if (write(clientSocket, &loginData, sizeof(struct userData)) < 0)
    {
        std::cout << "Can't send data to server ! [create account 3]" << std::endl;
        return ;
    }

    if (read(clientSocket, &result, sizeof(int)) < 0)
    {
        std::cout << "Can't read data from server ! [create account 3]" << std::endl;
        return ;
    }

    if (result == 1)
    {
        createAccountResult->setText("Account created !");
    }
    else if (result == 0)
    {
        createAccountResult->setText("An user with this name already exists !");
    }
    else
    {
        createAccountResult->setText("Can't create the account !");
    }
    result = -1;
}

void MainWindow::back()
{
    createAccountPanel->setVisible(false);
    loginPanel->setVisible(true);
}

void MainWindow::sendCommand()
{
    QString commandInputText = commandInput->text();
    QString commandAux = commandInputText;

    if (strlen(commandAux.toStdString().c_str()) > 99)
    {
        commandOutput->setText("Input can't have more than 99 characters !");
        return ;
    }

    int size = strlen(commandAux.toStdString().c_str());
    char *command = (char*) calloc(size, sizeof(char));
    strcpy(command, commandAux.toStdString().c_str());


    if (strcmp(command, "list") == 0)
    {
        if (write(clientSocket, &size, sizeof(int)) <= 0)
        {
            std::cout << "Can't send data to server ! [list]" << std::endl;
            return ;
        }

        if (write(clientSocket, command, sizeof(char) * size) <= 0)
        {
            std::cout << "Can't send data to server ! [list]" << std::endl;
            return ;
        }

        list();
    }
    else if (strcmp(command, "usersList") == 0 && isAdmin == 1)
    {
        if (write(clientSocket, &size, sizeof(int)) <= 0)
        {
            std::cout << "Can't send data to server ! [list]" << std::endl;
            return ;
        }

        if (write(clientSocket, command, sizeof(char) * size) <= 0)
        {
            std::cout << "Can't send data to server ! [list]" << std::endl;
            return ;
        }

        usersList();
    }
    else
    {
        char *downloadCommand = (char*) "download";
        char *aux1 = (char*) calloc(strlen(downloadCommand), sizeof(char));

        char *uploadCommand = (char*) "upload";
        char *aux2 = (char*) calloc(strlen(uploadCommand), sizeof(char));

        char *deleteFileCommand = (char*) "deleteFile";
        char *aux3 = (char*) calloc(strlen(deleteFileCommand), sizeof(char));

        char *deleteUserCommand = (char*) "deleteUser";
        char *aux4 = (char*) calloc(strlen(deleteUserCommand), sizeof(char));

        strncpy(aux1, command, 8);
        strncpy(aux2, command, 6);
        strncpy(aux3, command, 10);
        strncpy(aux4, command, 10);

        if (strcmp(downloadCommand, aux1) == 0)
        {
            int commandSize = strlen(downloadCommand);

            if (write(clientSocket, &commandSize, sizeof(int)) < 0)
            {
                std::cout << "Can't send data to server ! [download]" << std::endl;
                return ;
            }

            if (write(clientSocket, downloadCommand, sizeof(char) * commandSize) < 0)
            {
                std::cout << "Can't send data to server ! [download]" << std::endl;
                return ;
            }

            download(command + 9);
        }
        else if (strcmp(uploadCommand, aux2) == 0)
        {
            if (!selectedUploadPath)
            {
                commandOutput->setText("Please select a folder for upload !");
                return ;
            }

            int commandSize = strlen(uploadCommand);

            if (write(clientSocket, &commandSize, sizeof(int)) < 0)
            {
                std::cout << "Can't send data to server ! [upload]" << std::endl;
                return ;
            }

            if (write(clientSocket, uploadCommand, sizeof(char) * commandSize) < 0)
            {
                std::cout << "Can't send data to server ! [upload]" << std::endl;
                return ;
            }

            upload(command + 7);
        }
        else if (strcmp(deleteFileCommand, aux3) == 0)
        {
            int commandSize = strlen(deleteFileCommand);

            if (write(clientSocket, &commandSize, sizeof(int)) < 0)
            {
                std::cout << "Can't send data to server ! [deleteFile]" << std::endl;
                return ;
            }

            if (write(clientSocket, deleteFileCommand, sizeof(char) * commandSize) < 0)
            {
                std::cout << "Can't send data to server ! [deleteFile]" << std::endl;
                return ;
            }

            deleteFile(command + 11);
        }
        else if (strcmp(deleteUserCommand, aux4) == 0 && isAdmin == 1)
        {
            int commandSize = strlen(deleteUserCommand);

            if (write(clientSocket, &commandSize, sizeof(int)) < 0)
            {
                std::cout << "Can't send data to server ! [deleteUser]" << std::endl;
                return ;
            }

            if (write(clientSocket, deleteUserCommand, sizeof(char) * commandSize) < 0)
            {
                std::cout << "Can't send data to server ! [deleteUser]" << std::endl;
                return ;
            }

            deleteUser(command + 11);
        }

//        free(aux1);
//        free(aux2);
//        free(aux3);
//        free(aux4);
    }
}

void MainWindow::list()
{
    int size;

    if (read(clientSocket, &size, sizeof(int)) < 0)
    {
        std::cout << "Can't read data from server ! [list 1]" << std::endl;
        return ;
    }

    char* fileList = (char*) calloc(sizeof(char),  size);

    if (read(clientSocket, fileList, size * sizeof(char)) <= 0)
    {
        std::cout << "Can't read data from server ! [list 2]" << std::endl;
        return ;
    }

    char* showCommandOutput = (char*) malloc(sizeof(char) * 13);
    strcpy(showCommandOutput, "Files found:");

    int sizeOfOutput = sizeof(char) * (strlen(fileList) + strlen(showCommandOutput));
    showCommandOutput = (char*) realloc(showCommandOutput, sizeOfOutput);
    strcat(showCommandOutput, fileList);

    commandOutput->setText(QString::fromUtf8(showCommandOutput));

    free(showCommandOutput);
    free(fileList);
}

void MainWindow::usersList()
{
    int size;

    if (read(clientSocket, &size, sizeof(int)) < 0)
    {
        std::cout << "Can't read data from server ! [list 1]" << std::endl;
        return ;
    }

    char* list = (char*) calloc(sizeof(char),  size);

    if (read(clientSocket, list, size * sizeof(char)) <= 0)
    {
        std::cout << "Can't read data from server ! [list 2]" << std::endl;
        return ;
    }

    std::cout << "List: " << list << std::endl;

    char* showCommandOutput = (char*) malloc(sizeof(char) * 13);
    strcpy(showCommandOutput, "Users found:");

    int sizeOfOutput = sizeof(char) * (strlen(list) + strlen(showCommandOutput) + 1);
    showCommandOutput = (char*) realloc(showCommandOutput, sizeOfOutput);
    strcat(showCommandOutput, list);

    commandOutput->setText(QString::fromUtf8(showCommandOutput));

    if (strlen(showCommandOutput) > 1)
    {
        free(showCommandOutput);
    }
    free(list);
}

void MainWindow::download(char* fileName)
{
    int blocksOfData;
    int blockSize;
    int file;
    int rest;

    int size = strlen(fileName);

    char* filePath;

    if (selectedDownloadPath)
    {
        filePath = (char*) malloc(strlen(downloadPath) + strlen(fileName) + 3);
    }
    else
    {
        filePath = (char*) malloc(strlen(fileName) + 2);
    }

    if (write(clientSocket, &size, sizeof(int)) < 0)
    {
        std::cout << "Can't send data to server ! [download 1]" << std::endl;
        return ;
    }

    if (write(clientSocket, fileName, strlen(fileName) * sizeof(char)) < 0)
    {
        std::cout << "Can't send data to server ! [download 2]" << std::endl;
        return ;
    }

    if (read(clientSocket, &blockSize, sizeof(int)) < 0)
    {
        std::cout << "Can't send data to server ! [download 3]" << std::endl;
        return ;
    }

    if (blockSize == -1)
    {
        commandOutput->setText("File not found !");
        return ;
    }

    if (read(clientSocket, &blocksOfData, sizeof(int)) < 0)
    {
        std::cout << "Can't send data to server ! [download 4]" << std::endl;
        return ;
    }
    std::cout << "Pre test: " << std::endl;

    if (selectedDownloadPath == 1)
    {
        strcpy(filePath, downloadPath);
        strcat(filePath, "/");
        strcat(filePath, fileName);
        strcat(filePath, "\0");
    }
    else
    {
        strcpy(filePath, fileName);
    }

    if ((file = open(filePath, O_WRONLY | O_CREAT, 0777)) < 0)
    {
        std::cout << "Can't open file ! [download 5]" << std::endl;
        return ;
    }

    for (int i = 0; i < blocksOfData; i++)
    {
        char blockOfData[blockSize];

        if (read(clientSocket, blockOfData, blockSize) < 0)
        {
            std::cout << "Can't read data from server ! [download 6]" << std::endl;
            return ;
        }

        if (write(file, blockOfData, blockSize) < 0)
        {
            std::cout << "Can't write to file ! [download 7]" << std::endl;
            return ;
        }
    }

    if (read(clientSocket, &rest, sizeof(int)) < 0)
    {
        std::cout << "Can't read data from server ! [download 8]" << std::endl;
        return ;
    }

    char blockOfData[rest];

    if (read(clientSocket, blockOfData, rest) < 0)
    {
        std::cout << "Can't read data from server ! [download 9]" << std::endl;
        return ;
    }

    if (write(file, blockOfData, rest) < 0)
    {
        std::cout << "Can't write to file ! [download 10]" << std::endl;
        return ;
    }

    commandOutput->setText("Downlod complete !");

    ::close(file);
}

void MainWindow::upload(char* fileName)
{
    DIR *userDir = opendir(uploadPath);

    if (userDir)
    {
        struct dirent *fileData = NULL;
        int found = 0;
        char* path;
        int size;

        std::cout << "File name: " << fileName << std::endl;

        while((fileData = readdir(userDir)) && found == 0)
        {
            if (strcmp(fileData->d_name, fileName) == 0)
            {
                found = 1;
                struct stat fileStat;

                path = (char*) malloc(strlen(uploadPath) + strlen(fileName) + 2);

                strcpy(path, uploadPath);
                strcat(path, "/");
                strcat(path, fileName);
                strcat(path, "\0");

                std::cout << "Path: " << path << std::endl;

                if (stat(path, &fileStat) < 0)
                {
                    std::cout << "Error at stat !" << std::endl;
                }

                size = strlen(fileName);

                if (write(clientSocket, &size, sizeof(int)) < 0)
                {
                    std::cout << "Can't send data to server ! [upload 1]" << std::endl;
                    return ;
                }

                if (write(clientSocket, fileName, size * sizeof(char)) < 0)
                {
                    std::cout << "Can't send data to server ! [upload 2]" << std::endl;
                    return ;
                }

                int size = fileStat.st_size;

                int blockSize = 10240 * 5; // Every block will have 50 KB of data
                int blocksOfData = size / blockSize;

                if (write(clientSocket, &blockSize, sizeof(int)) < 0)
                {
                    std::cout << "Can't send data to server ! [upload 3]" << std::endl;
                    return ;
                }

                if (write(clientSocket, &blocksOfData, sizeof(int)) < 0)
                {
                    std::cout << "Can't send data to server ! [upload 4]" << std::endl;
                    return ;
                }

                int file;
                if ((file = open(path, O_RDONLY)) < 0)
                {
                    std::cout << "Can't open file ! [upload 5]" << std::endl;
                    commandOutput->setText("Can't open file or file not found !");
                }

                for (int i = 0; i < blocksOfData; i++)
                {
                    char blockOfData[blockSize];

                    read(file, blockOfData, blockSize);
                    write(clientSocket, &blockOfData, blockSize);
                }

                int rest = size - (blocksOfData * blockSize);
                char blockOfData[rest];

                write(clientSocket, &rest, sizeof(int));

                read(file, blockOfData, rest);
                write(clientSocket, &blockOfData, rest);

                ::close(file);
            }
        }

        if (!found)
        {
            commandOutput->setText("File not found !");
            return ;
        }

        commandOutput->setText("Upload complete !");

        closedir(userDir);
    }
}

void MainWindow::setDownloadLocation()
{
    QString aux = QFileDialog::getExistingDirectory(this, tr("Open Directory"), "/home",QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);
    int size = strlen(aux.toStdString().c_str()) * sizeof(char) + 1;

    downloadPath = (char*) malloc(size);
    strcpy (downloadPath, aux.toStdString().c_str());

    selectedDownloadPath = 1;
}

void MainWindow::setUploadLocation()
{
    QString aux = QFileDialog::getExistingDirectory(this, tr("Open Directory"), "/home",QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);
    int size = strlen(aux.toStdString().c_str()) * sizeof(char) + 1;

    uploadPath = (char*) malloc(size);
    strcpy (uploadPath, aux.toStdString().c_str());

    selectedUploadPath = 1;
}

void MainWindow::deleteFile(char *path)
{
    int size = strlen(path);
    int result = -1;

    if (write(clientSocket, &size, sizeof(int)) < 0)
    {
        std::cout << "Can't send data to server ! [deleteFile 1]" << std::endl;
        return ;
    }

    if (write(clientSocket, path, size * sizeof(char)) < 0)
    {
        std::cout << "Can't send data to server ! [deleteFile 2]" << std::endl;
        return ;
    }

    if (read(clientSocket, &result, sizeof(int)) < 0)
    {
        std::cout << "Can't read data from server ! [deleteFile 3]" << std::endl;
        return ;
    }

    if (result == 1)
    {
        commandOutput->setText("Operation completed !");
    }
    else
    {
        commandOutput->setText("Error !");
    }
}

void MainWindow::deleteUser(char *userName)
{
    int size = strlen(userName);
    int result = -1;

    if (write(clientSocket, &size, sizeof(int)) < 0)
    {
        std::cout << "Can't send data to server ! [deleteFile 1]" << std::endl;
        return ;
    }

    if (write(clientSocket, userName, size * sizeof(char)) < 0)
    {
        std::cout << "Can't send data to server ! [deleteFile 2]" << std::endl;
        return ;
    }

    if (read(clientSocket, &result, sizeof(int)) < 0)
    {
        std::cout << "Can't read data from server ! [deleteFile 3]" << std::endl;
        return ;
    }

    if (result == 1)
    {
        commandOutput->setText("User deleted !");
    }
    else
    {
        commandOutput->setText("Error !");
    }
}
