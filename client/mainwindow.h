#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include <QGroupBox>
#include <QLabel>
#include <QPushButton>
#include <QLineEdit>
#include <QFormLayout>
#include <QFileDialog>
#include <QVBoxLayout>

#include <iostream>
#include <string>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <dirent.h>

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);

private:

    int clientSocket;
    int isAdmin = 0;
    //char *ip = (char*)"79.115.83.205";
    char *ip = (char*)"127.0.0.1";
    int port = 3000;
    struct sockaddr_in clientAddress;

    //////////////////////// Login Panel ////////////////////////////

    QGroupBox *loginPanel;

    QLabel *usernameLogin;
    QLineEdit *usernameInput;

    QLabel *passwordLogin;
    QLineEdit *passwordInput;

    QFormLayout *loginLayout;

    QPushButton *loginButton;
    QPushButton *goToCreateAccountButton;

    QFileDialog *fileDialog;

    QLabel *loginResultLabel;

    ///////////////////////// Download Panel /////////////////////

    QGroupBox *downloadPanel;

    QLabel *commandText;
    QLabel *commandOutput;
    QLineEdit *commandInput;
    QPushButton *sendCommandButton;
    QPushButton *setDownloadLocationButton;
    QPushButton *setUploadLocationButton;

    QFormLayout *downloadPanelLayout;


    ////////////////////////// Create account /////////////////////////

    QGroupBox *createAccountPanel;
    QFormLayout *createAccountPanelLayout;

    QLabel *newUsernameLabel;
    QLineEdit *newUsernameInput;

    QLabel *newPasswordLabel;
    QLineEdit *newPasswordInput;

    QPushButton *createAccountButton;
    QPushButton *backButton;

    QLabel *createAccountResult;

    struct userData
    {
        char username[100];
        char password[100];
    };

    ////////////////////////////////////////////////////////////

    char* downloadPath;
    int selectedDownloadPath;

    char* uploadPath;
    int selectedUploadPath;

private slots:
    void login();
    void sendCommand();
    void setDownloadLocation();
    void setUploadLocation();
    void createAccount();
    void back();
    void goToCreateAccount();

    void list();
    void usersList();
    void download(char *fileName);
    void upload(char *fileName);
    void deleteFile(char *path);
    void deleteUser(char* userName);
};

#endif // MAINWINDOW_H
